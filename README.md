# Sign In with Apple

[Sign In with Apple](https://developer.apple.com/sign-in-with-apple/) allows
you to set up a user account in your system, complete with name, verified email
address, and unique stable identifiers that allow the user to sign in to your
app with their Apple ID. It works on iOS, macOS, tvOS, and watchOS. You can also
add Sign In with Apple to your website or versions of your app running on other
platforms. Once a user sets up their account, they can sign in anywhere you
deploy your app.

_PLEASE NOTE: Sign In with Apple was announced at WWDC 2019 in June 2019 and may
not be fully operational until iOS 13 is released in Fall 2019. This module will
likely not be fully operational until that time._

## Installation

1. Download and install the Sign In with Apple module.
1. Configure Sign In with Apple.
