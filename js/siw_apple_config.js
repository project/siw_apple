/**
 * @file
 * siw_apple_config.js
 */

AppleID.auth.init({
  clientId : '[CLIENT_ID]',
  scope : '[SCOPES]',
  redirectURI: '[REDIRECT_URI]',
  state : '[STATE]'
});

const buttonElement = document.getElementById('sign-in-with-apple-button');
buttonElement.addEventListener('click', () => {
  AppleID.auth.signIn();
});
